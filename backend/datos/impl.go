package datos

import (
	"todoapp/core"
	"todoapp/utils"
)

func ServicioTodo() core.TodoApp {
	var conf = utils.GetConfig()
	if conf.Storage == "mongo" {
		return &TodoMongo{}
	}
	return &TodoSql{}
}

func ServicioLogin() core.LoginApp {
	var conf = utils.GetConfig()
	if conf.Storage == "mongo" {
		return &LoginMongo{}
	}
	return &LoginSql{}
}
