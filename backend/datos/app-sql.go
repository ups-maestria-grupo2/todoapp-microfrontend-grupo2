package datos

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"strconv"
	"todoapp/core"
	"todoapp/utils"
)

type UsuarioDb struct {
	ID       int `gorm:"primaryKey"`
	Username string
	Password string
}

type TareaDb struct {
	ID        int `gorm:"primaryKey"`
	UserId    string
	Title     string
	Completed int
}

func (TareaDb) TableName() string {
	return "tarea"
}

func (UsuarioDb) TableName() string {
	return "usuario"
}

func (t TareaDb) mapear() core.Tarea {
	return core.Tarea{
		ID:       strconv.Itoa(t.ID),
		UserId:   t.UserId,
		Titulo:   t.Title,
		Completo: t.Completed == 1,
	}
}

func (t UsuarioDb) mapear() core.Usuario {
	return core.Usuario{
		ID:       strconv.Itoa(t.ID),
		Username: t.Username,
		Password: t.Password,
	}
}

func dbConnect() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("todoapp.db"), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	return db
}

type LoginSql struct {
}

func (LoginSql) Login(username string, password string) core.LoginRes {
	db := dbConnect()
	var user UsuarioDb
	var res core.LoginRes
	r := db.First(&user, "username = ?", username)
	if r.Error != nil {
		return *res.ConError("usuario invalido")
	}
	if !utils.PasswordVerify(password, user.Password) {
		return *res.ConError("credenciales incorrectas")
	}
	res.User = user.Username
	res.Id = strconv.Itoa(user.ID)
	return res
}

func (LoginSql) UpdateUser(username string, password string) (core.Usuario, error) {
	db := dbConnect()
	var user UsuarioDb
	user.Username = username
	r := db.FirstOrCreate(&user, "username = ?", username)
	if r.Error != nil {
		return core.Usuario{}, r.Error
	}
	user.Password = utils.HashPassword(password)
	db.Save(user)
	return user.mapear(), nil
}

func (LoginSql) ListaUsuarios() []string {
	db := dbConnect()
	allUsers := []UsuarioDb{}
	db.Find(&allUsers)
	var names []string
	for _, u := range allUsers {
		names = append(names, u.Username)
	}
	//var names []string
	//db.Model(&UsuarioDb{}).Pluck("username", &names)
	return names
}

// -----------------

// 	Insert(tarea TareaMg) TodoRes
//	Update(tarea TareaMg) TodoRes
//	Delete(id string) TodoRes
//	ListaTodos(user string) []Tarea

type TodoSql struct{}

func (TodoSql) Insert(tarea core.Tarea) core.TodoRes {
	db := dbConnect()
	rec := TareaDb{
		UserId:    tarea.UserId,
		Title:     tarea.Titulo,
		Completed: utils.UnoZero(tarea.Completo),
	}
	var res core.TodoRes
	r := db.Save(&rec)
	if r.Error != nil {
		res.Err = r.Error
		return res
	}
	res.Tarea = tarea
	res.Tarea.ID = strconv.Itoa(rec.ID)
	return res
}

func (TodoSql) Update(tarea core.Tarea) core.TodoRes {
	db := dbConnect()
	r := db.Model(&TareaDb{}).Where("id = ?", tarea.ID).Updates(TareaDb{
		Title:     tarea.Titulo,
		UserId:    tarea.UserId,
		Completed: utils.UnoZero(tarea.Completo),
	})
	var res core.TodoRes
	if r.Error != nil {
		res.Err = r.Error
		return res
	}
	res.Tarea = tarea
	return res
}

func (TodoSql) Delete(id string) core.TodoRes {
	db := dbConnect()
	db.Delete(&TareaDb{}, id)
	var res core.TodoRes
	return res
}

func (TodoSql) ListaTodos(userId string) []core.Tarea {
	db := dbConnect()
	var users []TareaDb
	//var mapeados []core.Tarea
	mapeados := make([]core.Tarea, 0)
	r := db.Where("user_id = ?", userId).Find(&users)
	if r.Error != nil {
		panic(r.Error)
	}
	for _, u := range users {
		mapeados = append(mapeados, u.mapear())
	}
	return mapeados
}
