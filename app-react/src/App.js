// import './App.css';
import React, {useMemo, useState} from 'react';
import {alerta, appUrl, safePublish} from "./utils";

const axios = require('axios').default;

function App() {

    const [username, setUsername] = useState('');
    const changeUser = (e) => setUsername(e.target.value);
    const [password, setPassword] = useState('');
    const changePassword = (e) => setPassword(e.target.value);
    const [message, setMessage] = useState('');

    const [loggedin, setLoggedin] = useState(false);

    function logout() {
        console.log('logout')
        setLoggedin(false)
        window.sessionStorage.removeItem('user')
        //getBus().publicar('logout')
        safePublish('logout')
    }

    function login() {
        console.log('login')
        if (!username || !password) {
            setMessage("Por favor complete el formulario");
            return false;
        }
        setMessage("Consultando...");

        var data = {username: username, password: password};
        axios.post(appUrl() + '/login', data).then(r => {
            let res = r.data
            setUsername('')
            setPassword('')
            setMessage("");
            setLoggedin(true)
            window.sessionStorage.setItem('user', res.user)
            //getBus().publicar('login', res)
            safePublish('login', res)
        }).catch(error => {
            console.log(error);
            if (error.response) {
                let d = error.response.data
                if (error.response.status == 400) {
                    setMessage("Credenciales invalidas")
                } else
                    setMessage("ERROR de comunicacion")
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else {
                setMessage("ERROR")
                alerta(error);
            }
        })
    }

    function ping() {
        axios.get(appUrl() + '/ping').then(r => {
            alerta(r.data)
            console.log(r.data)
        })
    }

    // init
    useMemo(() => {
        let user = window.sessionStorage.getItem('user');
        if (user) {
            console.log("SI HAY USUARIO")
            setLoggedin(true)
            safePublish('login', {user: user})
        }
    }, [])

    function prueba() {
        safePublish("prueba", {nombre: 'gato'})
    }

    return (
        <div className="App">
            {!loggedin &&
                <form id="formLogin" action="" autoComplete="off">
                    <div className="mb-3">
                        <label class="form-label">Username</label>
                        <input name="username" value={username} onChange={changeUser} required/>
                    </div>
                    <br/>
                    <div className="mb-3">
                        <label class="form-label">Password</label>
                        <input name="password" type="password" value={password} onChange={changePassword} required/>
                    </div>

                    <button type="button" onClick={login}>Login</button>
                    <br/>
                    <label>{message}</label>
                </form>
            }
            {loggedin &&
                <button type="button" onClick={logout}>Logout</button>
            }
            {/*<hr/>*/}
            {/*<button type="button" onClick={prueba}>PRUEBA</button>*/}
        </div>
    );
}

export default App;
