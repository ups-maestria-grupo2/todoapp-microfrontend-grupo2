import './App.css';

function App() {

    function root() {
        var p = window.location.href.split('#');
        return p[0];
    }

    function recargar(hash) {
        var p = window.location.href.split('?');
        window.location.href = p[0] + '?modo=' + hash;
        return true;
    }

    return (
        <div className="App">
            <h3>TODOs app - GRUPO 2</h3>

            <div>
                <button onClick={() => recargar('iframes')}>Iframes</button> |
                <button onClick={() => recargar('js')}>JS</button>
            </div>

        </div>
    );
}

export default App;
