package datos

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"todoapp/core"
	"todoapp/utils"
)

type TareaMg struct {
	ID       primitive.ObjectID `bson:"_id" json:"id"`
	Titulo   string             `bson:"titulo,omitempty" json:"titulo"`
	Completo bool               `bson:"completo,omitempty" json:"completo"`
	UserId   string             `bson:"user_id,omitempty" json:"user_id"`
}

type UsuarioMg struct {
	ID       primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Username string             `bson:"username,omitempty" json:"username"`
	Password string             `bson:"password,omitempty" json:"password"`
}

func (u *UsuarioMg) ToBson() bson.D {
	return bson.D{{"username", u.Username}, {"password", u.Password}}
}

func (u *TareaMg) ToBson() bson.D {
	return bson.D{{"titulo", u.Titulo},
		{"completo", u.Completo},
		{"user_id", u.UserId},
	}
}

func getMongoClient() (*mongo.Client, error) {
	conn := utils.GetEnv("MONGO_URI", "mongodb://localhost:27017")
	//client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	return mongo.Connect(context.TODO(), options.Client().ApplyURI(conn))
}

func TestMongo() bool {
	conn := utils.GetEnv("MONGO_URI", "mongodb://localhost:27017")
	//client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(conn))
	if err != nil {
		panic(err)
		return false
	}
	if err := client.Ping(context.TODO(), readpref.Primary()); err != nil {
		panic(err)
	}
	return true
}

type LoginMongo struct{}

func (LoginMongo) Login(username string, password string) core.LoginRes {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("usuarios")

	var res core.LoginRes
	var user UsuarioMg
	filter := bson.D{{"username", username}}
	err := col.FindOne(context.TODO(), filter).Decode(&user)
	if err != nil {
		res.Err = err
		return res
	}
	res.User = user.Username
	if !utils.PasswordVerify(password, user.Password) {
		res.Err = errors.New("credenciales invalidas")
	}
	res.Id = user.ID.Hex()
	res.User = user.Username
	return res
}

func (LoginMongo) UpdateUser(username string, password string) (core.Usuario, error) {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("usuarios")

	opts := options.Update().SetUpsert(true)

	var user UsuarioMg
	user.Username = username

	hashedPassword := utils.HashPassword(password)
	user.Password = hashedPassword

	filter := bson.D{{"username", username}}
	update := bson.D{{"$set", user.ToBson()}}

	result, err := col.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		panic(err)
	}
	//https://stackoverflow.com/questions/59537212/how-to-convert-mongo-insertoneresults-insertedid-to-byte
	if result.UpsertedID != nil {
		user.ID = result.UpsertedID.(primitive.ObjectID)
	}
	return core.Usuario{
		ID:       user.ID.Hex(),
		Username: user.Username,
		Password: user.Password,
	}, nil
}

func (LoginMongo) ListaUsuarios() []string {
	res := make([]string, 0)
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("usuarios")

	cursor, err := col.Find(context.TODO(), bson.D{})
	if err != nil {
		return res
	}
	defer func() {
		cursor.Close(context.Background())
	}()
	for cursor.Next(context.TODO()) {
		//Create a value into which the single document can be decoded
		var elem UsuarioMg
		err := cursor.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		res = append(res, elem.Username)
	}
	return res
}

// ---------------------------------------------------------

type TodoMongo struct{}

func upsertTarea(todo core.Tarea) core.Tarea {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("tareas")

	var user TareaMg
	user.Titulo = todo.Titulo
	user.Completo = todo.Completo
	user.UserId = todo.UserId

	log.Printf("Completo %s", todo.Completo)

	ctx := context.TODO()
	if todo.ID == "" {
		resi, err := col.InsertOne(ctx, user.ToBson())
		if err != nil {
			panic(err)
		}
		todo.ID = resi.InsertedID.(primitive.ObjectID).Hex()
	} else {
		objID, _ := primitive.ObjectIDFromHex(todo.ID)
		filter := bson.M{"_id": bson.M{"$eq": objID}}
		update := bson.D{{"$set", user.ToBson()}}
		//update := bson.M{"$set": bson.M{"completo": true}}
		_, err := col.UpdateOne(
			context.Background(),
			filter,
			update,
		)
		if err != nil {
			panic(err)
		}
	}
	return todo
}

func (TodoMongo) Insert(tarea core.Tarea) core.TodoRes {
	var res core.TodoRes
	tt := upsertTarea(tarea)
	res.Tarea = tt
	return res
}

func (TodoMongo) Update(tarea core.Tarea) core.TodoRes {
	var res core.TodoRes
	tt := upsertTarea(tarea)
	res.Tarea = tt
	return res
}

func (TodoMongo) Delete(id string) core.TodoRes {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("tareas")

	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	filter := bson.D{primitive.E{Key: "_id", Value: oid}}

	_, err = col.DeleteOne(context.TODO(), filter)
	if err != nil {
		panic(err)
	}
	//return res.DeletedCount > 0
	return core.TodoRes{}
}

func (TodoMongo) ListaTodos(userId string) []core.Tarea {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("tareas")
	filter := bson.D{{"user_id", userId}}
	//cursor, err := col.Find(context.TODO(), bson.D{{"username", userId}})
	cursor, err := col.Find(context.TODO(), filter)
	lista := make([]core.Tarea, 0)
	if err != nil {
		return lista
	}
	for cursor.Next(context.TODO()) {
		var elem TareaMg
		err := cursor.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}
		lista = append(lista, core.Tarea{
			ID:       elem.ID.Hex(),
			UserId:   userId,
			Titulo:   elem.Titulo,
			Completo: elem.Completo,
		})
	}
	return lista
}
