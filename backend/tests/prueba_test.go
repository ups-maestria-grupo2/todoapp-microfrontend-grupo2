package tests

import (
	"fmt"
	"testing"
	"todoapp/core"
	"todoapp/datos"
)

func TestUserMongo(tt *testing.T) {
	s := datos.LoginMongo{}
	res, err := s.UpdateUser("prueba", "prueba")
	if err != nil {
		tt.Errorf("error %s", err.Error())
	}
	fmt.Println(res.ID)
}

func TestSql(tt *testing.T) {
	ser := datos.TodoSql{}
	t := core.Tarea{
		ID:       "",
		UserId:   "1",
		Titulo:   "testing",
		Completo: false,
	}
	res := ser.Insert(t)

	if res.Err != nil {
		tt.Errorf("error %s", res.Err.Error())
	}

	if res.Tarea.ID == "" {
		tt.Error("sin id")
	}
}

func TestMongo(tt *testing.T) {
	ser := datos.TodoMongo{}
	t := core.Tarea{
		ID:       "",
		UserId:   "prueba",
		Titulo:   "testing",
		Completo: false,
	}
	res := ser.Insert(t)

	if res.Err != nil {
		tt.Errorf("error %s", res.Err.Error())
	}

	if res.Tarea.ID == "" {
		tt.Error("sin id")
	}
	fmt.Printf("Id: %s\n", res.Tarea.ID)
}
