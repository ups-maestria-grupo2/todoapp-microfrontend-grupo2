package datos

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func DeleteTarea(id string) bool {
	client, _ := getMongoClient()
	col := client.Database("todos").Collection("usuarios")

	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	filter := bson.D{primitive.E{Key: "_id", Value: oid}}

	res, err := col.DeleteOne(context.TODO(), filter)
	if err != nil {
		panic(err)
	}
	return res.DeletedCount > 0
}
