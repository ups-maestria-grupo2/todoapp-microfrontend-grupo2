package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"reflect"
	"todoapp/core"
	"todoapp/datos"
	"todoapp/utils"
)

type LoginForm struct {
	User     string `form:"user" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func main() {
	fmt.Println("TODOAPP con Microfrontends - Grupo 2")
	errConf := godotenv.Load(".env")
	if errConf != nil {
		log.Fatalf("Error archivo de configuracion. Err: %s", errConf)
	}

	//datos.TestMongo()
	conf := utils.GetConfig()
	fmt.Println(conf.Storage)
	fmt.Println(conf.Port)
	fmt.Println(reflect.TypeOf(datos.ServicioLogin()))

	router := gin.Default()
	//https://stackoverflow.com/questions/69608544/enable-cors-policy-in-golang
	router.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"POST", "PUT", "PATCH", "DELETE"},
		AllowHeaders: []string{"Content-Type,access-control-allow-origin, access-control-allow-headers"},
	}))

	router.Use(gin.Recovery())
	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
		//c.JSON(http.StatusOK, gin.H{"message": "pong"})
	})

	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "TODOApp v1.0 Grupo 2")
	})

	router.GET("/users", func(c *gin.Context) {
		ser := datos.ServicioLogin()
		lista := ser.ListaUsuarios()
		c.JSON(http.StatusOK, lista)
	})

	router.GET("/config", func(c *gin.Context) {
		conf := utils.GetConfig()
		c.JSON(http.StatusOK, conf)
	})

	router.PATCH("/reload", func(c *gin.Context) {
		errConf := godotenv.Load(".env")
		if errConf != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": errConf.Error()})
			return
		}
		c.JSON(http.StatusOK, "OK")
	})

	router.POST("/login", func(c *gin.Context) {
		var form LoginForm
		if err := c.ShouldBindJSON(&form); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-bind": err.Error()})
			return
		}

		service := datos.ServicioLogin()
		loginRes := service.Login(form.User, form.Password)
		if loginRes.Err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-serv": loginRes.Err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"id":   loginRes.Id,
			"user": loginRes.User,
		})
	})

	router.POST("/adduser", func(c *gin.Context) {
		var form LoginForm
		if err := c.ShouldBindJSON(&form); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-bind": err.Error()})
			return
		}

		service := datos.ServicioLogin()
		log.Println(reflect.TypeOf(service))
		user, err := service.UpdateUser(form.User, form.Password)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-serv": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"id":   user.ID,
			"user": user.Username,
		})
	})

	router.GET("/todos/:user", func(c *gin.Context) {
		user := c.Param("user")
		ser := datos.ServicioTodo()
		lista := ser.ListaTodos(user)
		c.JSON(http.StatusOK, lista)
	})

	router.PUT("/todos", func(c *gin.Context) {
		var t core.Tarea
		if err := c.ShouldBindJSON(&t); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-bind": err.Error()})
			return
		}

		ser := datos.ServicioTodo()
		res := ser.Insert(t)
		if res.Err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-serv": res.Err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"id": res.Tarea.ID})
	})

	router.POST("/todos", func(c *gin.Context) {
		var t core.Tarea
		if err := c.ShouldBindJSON(&t); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-bind": err.Error()})
			return
		}

		ser := datos.ServicioTodo()
		res := ser.Update(t)
		if res.Err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error-serv": res.Err.Error()})
			return
		}
		c.JSON(http.StatusOK, gin.H{"id": res.Tarea.ID})
	})

	router.DELETE("/todos/:id", func(c *gin.Context) {
		id := c.Param("id")
		ser := datos.ServicioTodo()
		res := ser.Delete(id)
		if res.Err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error-serv": res.Err.Error()})
			return
		}
		c.JSON(http.StatusOK, "OK")
	})

	// listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	err := router.Run()
	if err != nil {
		log.Fatalf("Error ")
	}
}
