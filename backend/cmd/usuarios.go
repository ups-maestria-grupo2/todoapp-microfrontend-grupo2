package main

import (
	"flag"
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"strings"
	"todoapp/datos"
	"todoapp/utils"
)

func main() {
	// linea de comandos para mantenimiento de usuarios rapido
	var username string
	var pass string
	var lista bool
	var login bool

	errConf := godotenv.Load(".env")
	if errConf != nil {
		log.Fatalf("Error archivo de configuracion. Err: %s", errConf)
	}
	fmt.Println("TODOApp gestion")

	// flags declaration using flag package
	flag.StringVar(&username, "u", "", "Username para el usuario")
	flag.StringVar(&pass, "p", "", "Password para el usuario")
	flag.BoolVar(&lista, "l", false, "lista")
	flag.BoolVar(&login, "login", false, "test login")
	flag.Parse() // after declaring flags we need to call it

	ser := datos.ServicioLogin()
	fmt.Printf("Storage: %s\n", utils.GetConfig().Storage)

	if lista {
		fmt.Printf("Usuarios:")
		lista := ser.ListaUsuarios()
		for _, v := range lista {
			fmt.Println(v)
		}
		os.Exit(0)
	}

	username = strings.TrimSpace(username)
	pass = strings.TrimSpace(pass)

	// check if cli params match
	if username == "" || pass == "" {
		fmt.Printf("Datos invalidos, use -p y -u")
		os.Exit(1)
	}

	if login {
		res := ser.Login(username, pass)
		if res.Err != nil {
			fmt.Printf("Error: %s", res.Err.Error())
		} else {
			fmt.Printf("EXITO LOGIN")
		}
		return
	}

	_, err := ser.UpdateUser(username, pass)
	if err != nil {
		fmt.Printf("error %s", err)
	} else {
		fmt.Printf("Usuario actualizado %s", username)
	}

}
