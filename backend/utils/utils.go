package utils

import (
	"golang.org/x/crypto/bcrypt"
	"os"
)

type Config struct {
	Storage string
	Port    string
}

func GetConfig() Config {
	conf := Config{}
	conf.Storage = GetEnv("STORAGE", "sqlite")
	conf.Port = GetEnv("PORT", "")
	return conf
}

// GetEnv Obtener variable de entorno con default
func GetEnv(key, fallback string) string {
	//val := os.Getenv("STACK")
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// https://github.com/sohamkamani/go-password-auth-example/blob/master/handlers.go
func HashPassword(password string) string {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(password), 8)
	return string(hashedPassword)
}

func PasswordVerify(password string, hash string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)); err != nil {
		return false
	}
	return true
}

func UnoZero(bool2 bool) int {
	if bool2 {
		return 1
	}
	return 0
}
