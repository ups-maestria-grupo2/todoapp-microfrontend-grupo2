class BusWindow {

    publicarEvento(evento, eventData) {
        console.log(`:: DISPATCHING ${evento}`)
        let data = {};
        if (eventData)
            data.detail = eventData
        const customEvent = new CustomEvent(evento, data)
        let container = getContainer();
        container.dispatchEvent(customEvent)
    }

    suscribirseEvento(evento, callback) {
        let container = getContainer();
        container.addEventListener(evento, callback)
    }

}

export function safePublish(evento, data) {
    window.parent.postMessage({event: evento, detail: data}, "*")
    console.log(":: publico " + evento)
}

export function safeSub(evento, callback) {
    console.log(":: sub a " + evento)
    window.addEventListener("message", function (e) {
        console.log("LLEGO ALGO");
        console.log(e);
        if (e['event'] !== evento)
            return;
        console.log(":: consume " + evento)
        callback(e)
    });
}

function publicarEvento(evento, eventData) {
    let data = {};
    if (eventData)
        data.detail = eventData
    const customEvent = new CustomEvent('custom-event', data)
    let container = getContainer();
    container.dispatchEvent(customEvent)
}

function suscribirseEvento(evento, callback) {
    let container = getContainer();
    container.addEventListener(evento, callback)
}

export function getContainer() {
    if (window.location !== window.parent.location)
        return window.parent;
    return window;
}

/**
 * @returns null|BusWindow
 */
export function getBus() {
    let container = getContainer();
    if (!container['bus'])
        container.bus = new BusWindow()
    return container.bus;
}

export function getApp() {
    let container = getContainer();
    return container.app;
}

export function appUrl() {
    let def = 'http://localhost:8080';
    return window.appUrl || def;
}

export function alerta(msg) {
    window.alert(msg);
}

export {publicarEvento, suscribirseEvento}