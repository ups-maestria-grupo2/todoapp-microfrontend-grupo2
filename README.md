# Todoapp Microfrontends Grupo2

### Universidad Poliltecnica Salesiana - Maestria de Software
### Desarrollo de Aplicaciones Empresariales

Grupo 2:
- Daniel Burbano
- José Castillo
- Klever Simaliza
- Carlos Párraga
- Manuel Gómez

Tema: Demo Micro-frontends

# API Backend:

### Direccion:

Base URI: http://ec2-54-189-61-175.us-west-2.compute.amazonaws.com:8080

### MODELOS:

Usuario:
`
{
"username" : "",
"password" : ""
}
    `

Tarea (todo) :
`
{
"id" : "",
"user_id" : "",
"titulo" : "",
"completo" : false
}
`

Para las operaciones con Todos, sae recomienda poner el nombre de usuario en
user_id.

### ENDPOINTS:
in = input en json, out = output en json

**GET    /** 

Inicio
- out: "TODOApp v1.0 Grupo 2"

**GET    /ping**

Check de conexion
- out: "pong"

**GET    /users** 

Lista de usuarios registrados
- out: `[]Usuario`

**GET    /config** 

Debug configuracion
- out: `{"Storage":"sqlite/mongo","Port":"8080"}`

**PATCH  /reload** 

Recargar config (probando)
- out : "OK"

**POST   /login** 

Validar credenciales de usuario
- in: `Usuario`
- out: `{"id":"id-usuario", "user":"username"}`

**POST   /adduser** 

Crear/Actualizar usuario
- in: Usuario
- out: `{"id":"id-usuario", "user":"username"}`
 
**GET    /todos/:username** 

Lista de todos de un usuario
- out: `[]Tarea`

**PUT    /todos** 

Crear un todo
- in: `Tarea`
- out: `{"id":"id-tarea-creada"}`

**POST   /todos** 

Actualizar un todo
- in: `Tarea`
- out: `{"id":"id-tarea-actualizada"}`

**DELETE /todos/:id-tarea** 

Eliminar un todo
- out: "OK"

### Referencias:
- https://jsonplaceholder.typicode.com/todos/
- https://gorm.io/
- https://github.com/bcerati/js-event-bus