package core

import "errors"

type Tarea struct {
	ID       string `json:"id"`
	UserId   string `json:"user_id"`
	Titulo   string `json:"titulo"`
	Completo bool   `json:"completo"`
}

type Usuario struct {
	ID       string `json:"_id"`
	Username string `json:"email"`
	Password string `json:"password"`
}

type LoginRes struct {
	Id   string
	User string
	Err  error
}

func (l *LoginRes) ConError(err string) *LoginRes {
	l.Err = errors.New(err)
	return l
}

type TodoApp interface {
	Insert(tarea Tarea) TodoRes
	Update(tarea Tarea) TodoRes
	Delete(id string) TodoRes
	ListaTodos(userId string) []Tarea
}

// TodoRes resultado de operacion unica
type TodoRes struct {
	Tarea Tarea
	Err   error
}

type LoginApp interface {
	Login(username string, password string) LoginRes
	UpdateUser(username string, password string) (Usuario, error)
	ListaUsuarios() []string
}
